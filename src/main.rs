use bit_iter::*;
use std::{fs, u8};

fn load_functions(file_contents: Vec<u8>) -> Vec<Vec<bool>> {
    let mut functions: Vec<Vec<bool>> = Vec::new();
    for _ in 0..8 {
        functions.push(Vec::new());
    }

    for i in 0..file_contents.len() {
        if i % 2 == 1 {
            continue;
        }

        let current_byte = file_contents.get(i).unwrap();
        let ones: Vec<usize> = BitIter::from(*current_byte).collect();

        for i in 0..8 {
            let current_function = functions.get_mut(i).unwrap();
            if ones.contains(&i) {
                current_function.push(true);
            } else {
                current_function.push(false);
            }
        }
    }

    return functions;
}

fn count_ones(fun: &Vec<bool>) -> i32 {
    let mut ones = 0;
    for bit in fun.iter() {
        if *bit {
            ones += 1;
        }
    }
    return ones;
}

fn check_balances(functions: &Vec<Vec<bool>>, print: bool) {
    for fun in functions.iter() {
        let ones = count_ones(fun);
        let zeros = fun.len() as i32 - ones;
        if print {
            println!("Ones: {}, zeros: {}", ones, zeros);
        }
        //assert!(zeros == ones);
    }
}

fn hamming_distance(function_a: &Vec<bool>, function_b: &Vec<bool>) -> i32 {
    let mut result: Vec<bool> = Vec::new();

    for x in 0..function_a.len() {
        let val_a = function_a.get(x).unwrap();
        let val_b = function_b.get(x).unwrap();
        result.push(val_a ^ val_b);
    }

    return count_ones(&result);
}

fn generate_combination_values() -> Vec<u8> {
    let mut combinations = Vec::new();
    for x in 0u8..=255 {
        combinations.push(x);
    }
    return combinations;
}

fn generate_combinations() -> Vec<Vec<usize>> {
    let mut combinations = Vec::new();
    for x in 0u8..=255 {
        combinations.push(BitIter::from(x).collect());
    }
    return combinations;
}

fn generate_liniar_function(function: &Vec<bool>, indexes: Vec<usize>) -> Vec<bool> {
    let mut liniar = Vec::new();

    for lin_index in 0..function.len() {
        let mut values: Vec<bool> = Vec::new();
        let ones: Vec<usize> = BitIter::from(lin_index as u8).collect();
        for index in indexes.iter() {
            if ones.contains(index) {
                values.push(true);
            } else {
                values.push(false);
            }
        }
        let mut sum = false;
        for value in values.iter() {
            sum ^= value;
        }
        liniar.push(sum);
    }
    return liniar;
}

fn get_nonliniarities(functions: &Vec<Vec<bool>>) -> Vec<i32> {
    let mut minimal_distances = Vec::new();

    for _ in 0..8 {
        minimal_distances.push(u8::MAX as i32);
    }

    for comb in generate_combinations().iter() {
        for fun_index in 0..functions.len() {
            let fun = functions.get(fun_index).unwrap();
            let liniar = generate_liniar_function(fun, comb.to_owned());
            let curr_minimal = minimal_distances.get_mut(fun_index).unwrap();
            let d = hamming_distance(fun, &liniar);
            if *curr_minimal > d {
                *curr_minimal = d;
            }
        }
    }
    return minimal_distances;
}

fn compare_value(index: u8, function: &Vec<bool>) -> Vec<bool> {
    let combinations = generate_combination_values();
    let original_value = function.get(index as usize).unwrap().to_owned();
    let mut compared: Vec<bool> = Vec::new();

    for combination in combinations.iter() {
        let function_value = function.get(*combination as usize).unwrap();
        compared.push(original_value == *function_value);
    }
    return compared;
}

fn get_sac(function: &Vec<bool>) -> f64 {
    let mut ones_in_compares: Vec<bool> = Vec::new();
    for i in 0u8..=255u8 {
        let mut compared = compare_value(i, function);
        ones_in_compares.append(&mut compared);
    }

    let ones = count_ones(&ones_in_compares) as f64;
    return ones / ones_in_compares.len() as f64;
}

fn get_sacs(functions: &Vec<Vec<bool>>) -> Vec<f64> {
    let mut sacs: Vec<f64> = Vec::new();

    for fun in functions.iter() {
        sacs.push(get_sac(fun));
    }

    return sacs;
}

fn main() {
    let file_contents = fs::read("sbox.sbx").unwrap();
    assert!(file_contents.len() == 256 * 2);

    let functions = load_functions(file_contents);
    check_balances(&functions, true);
    let nonliniarities = get_nonliniarities(&functions);
    println!("Nonliniarities: {:?}", nonliniarities);
    let sacs = get_sacs(&functions);
    println!("Sacs: {:?}", sacs);
}
